package main

import "time"

const (
	INC = 1
	DEC = -1
)

type Category struct {
	Name        string // necessary
	Type        int    // necessary
	Description string // not necessary
}

type Operation struct {
	ID          int
	Category    string // necessary
	Cost        int    // necessary
	Who         string
	When        time.Time
	Description string // not necessary
}
