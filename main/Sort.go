package main

func sort(arr []int) {

	for i := 0; i < len(arr)-1; i++ {
		if arr[i] > arr[i+1] {
			j := 0
			for arr[i+1] > arr[j] {
				j++
			}
			temp := arr[i+1]
			move(j, i, arr)
			arr[j] = temp
		}
	}

}

func move(lo int, hi int, arr []int) {
	for i := hi; i >= lo; i-- {
		arr[i+1] = arr[i]
	}
}
