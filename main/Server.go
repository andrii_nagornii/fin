package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

var categories []Category

func main() {
	fmt.Println("Start server")

	categories = loadTypes()

	balance := calcBalance()
	fmt.Println("Balance = " + strconv.Itoa(balance))

	http.HandleFunc("/add_category", func(w http.ResponseWriter, r *http.Request) {
		fmt.Print("Try add category : ")
		bodyAsBytes, _ := ioutil.ReadAll(r.Body)
		body := string(bodyAsBytes)
		fmt.Print(body)
		type0 := createCategory(body)
		saveCategory(type0)
		categories = append(categories, type0)
		fmt.Println("Ok")
	})

	http.HandleFunc("/add_operation", func(w http.ResponseWriter, r *http.Request) {
		fmt.Print("Try add operation : ")
		bodyAsBytes, _ := ioutil.ReadAll(r.Body)
		body := string(bodyAsBytes)
		fmt.Print(body)
		operation := createOperation(body)
		saveOperation(operation)
		fmt.Println(" Ok")
	})

	http.HandleFunc("/get_categories", func(w http.ResponseWriter, r *http.Request) {
		fmt.Print("Try get categories....")
		result := "|"
		for _, category := range categories {
			categoryAsString := category.Name + ";" + strconv.Itoa(category.Type) + ";" + category.Description
			result += categoryAsString + "|"
		}

		fmt.Fprintln(w, result)
		fmt.Println("Ok")
	})

	http.HandleFunc("/get_balance", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Try get balance....")

		balance := calcBalance()
		fmt.Println("Balance = " + strconv.Itoa(balance))

		fmt.Fprintln(w, balance)
		fmt.Println("Ok")
	})

	log.Fatal(http.ListenAndServeTLS(":8080", "localhost.crt", "localhost.key", nil))

}

func createCategory(msg string) Category {
	typeAsString := strings.Split(msg, ";")
	type0, _ := strconv.Atoi(typeAsString[1])
	category := Category{typeAsString[0], type0, typeAsString[2]}
	return category
}

func createOperation(msg string) Operation {
	operationAsString := strings.Split(msg, ";")

	cost, _ := strconv.Atoi(operationAsString[1])

	operation := Operation{
		0,
		operationAsString[0],
		cost,
		operationAsString[2],
		time.Now(),
		operationAsString[3],
	}

	return operation
}

func createOperation0(msg string) Operation {
	operationAsString := strings.Split(msg, ";")

	cost, _ := strconv.Atoi(operationAsString[2])

	operation := Operation{
		0,
		operationAsString[1],
		cost,
		operationAsString[3],
		time.Now(),
		operationAsString[4],
	}

	return operation
}

func convertTypeToStr(category Category) string {
	return category.Name + ";" + strconv.Itoa(category.Type) + ";" + category.Description
}

func convertOperationToStr(operation Operation) string {
	return strconv.Itoa(operation.ID) + ";" + operation.Category + ";" + strconv.Itoa(operation.Cost) + ";" + operation.Who + ";" + operation.When.String() + ";" + operation.Description
}

func saveCategory(category Category) {
	f, _ := os.OpenFile("types", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)

	typeAsStr := convertTypeToStr(category)

	f.WriteString(typeAsStr + "\n")
	f.Close()
}

func saveOperation(operation Operation) {
	f, _ := os.OpenFile("operations", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)

	operationAsStr := convertOperationToStr(operation)

	f.WriteString(operationAsStr + "\n")
	f.Close()
}

func loadTypes() []Category {
	result := make([]Category, 0)
	file, _ := os.OpenFile("types", os.O_RDONLY|os.O_CREATE, 0666)
	dat, _ := ioutil.ReadFile(file.Name())
	if len(dat) != 0 {
		typesAsStr := strings.Split(string(dat), "\n")
		for _, typeAsStr := range typesAsStr {
			if typeAsStr == "" {
				break
			}
			type0 := createCategory(typeAsStr)
			result = append(result, type0)
		}
	}

	return result
}

func calcBalance() int {
	result := 0
	file, _ := os.OpenFile("operations", os.O_RDONLY, 0666)
	dat, _ := ioutil.ReadFile(file.Name())
	if len(dat) != 0 {
		operationsAsStr := strings.Split(string(dat), "\n")
		for _, typeAsStr := range operationsAsStr {
			if typeAsStr == "" {
				break
			}
			operation := createOperation0(typeAsStr)

			categoryType := getCategoryType(operation.Category)

			if categoryType == INC {
				result += operation.Cost
			} else if categoryType == DEC {
				result -= operation.Cost
			} else {
				//TODO ?????
			}
		}
	}

	return result
}

func getCategoryType(name string) int {
	for i := range categories {
		if categories[i].Name == name {
			return categories[i].Type
		}
	}
	return 0
}
